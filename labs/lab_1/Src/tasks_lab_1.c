/*
 * tasks_lab_1.c
 *
 *  Created on: Jan 31, 2019
 *      Author: Maciej
 */
#include "tasks_lab_1.h"
#include "../Drivers/RTS_libs/FEAT_Scheduler/sch_basic_pub.h"

#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_hal_uart.h"
#include <string.h>

extern ADC_HandleTypeDef hadc1;
extern UART_HandleTypeDef huart2;

#define LED_G_PORT GPIOA
#define LED_G_PIN    GPIO_PIN_5

#define TX_BUFFER_SIZE 300
static uint8_t tx_buffer[TX_BUFFER_SIZE];
static volatile int tx_head = 0, tx_tail = 0;

static volatile int led_val = 0;

static volatile int last_trigger = 0;

// helper function for transmitting strings
int uartPrintString(char *str, int length);

// periodic functions
void transmitUARTByte();
void read_ADC();
void check_UART();
void check_input();
void toggleLED();

// aperiodic functions
void transmitHelpString();

// sporadic functions
void respondToInput();

void lab1_power_up()
{

}

void lab1_init()
{
    // add periodic task for ADC
    addPeriodicTask(&check_input, 0, soft, 500, 1);
    addPeriodicTask(&toggleLED, 6, soft, 500, 0);
    addPeriodicTask(&check_UART, 5, soft, 50, 0);
    addPeriodicTask(&read_ADC, 4, soft, 100, 0);

    addPeriodicTask(&transmitUARTByte, 3, soft, 200, 1);

    char msg[] = "lab1 initialized\n\r";
    uartPrintString(msg, strlen(msg));
}

void toggleLED() {
    HAL_GPIO_WritePin(LED_G_PORT, LED_G_PIN, led_val);
    led_val = ~led_val & 0x01;
}

void check_input()
{
    if (GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13))
    {
        addSporadicJob(&respondToInput, 10, 0);
    }
}

uint8_t buf[10];
uint16_t buf_len = 1; // reading one char at a time
void check_UART()
{
    if (HAL_OK == HAL_UART_Receive(&huart2, buf, buf_len, 0))
    {
        // receive successful a byte
        if ((buf[0]=='t')||(buf[0] == 'T'))
        {
            // Start temperature reading from ADC
            HAL_ADC_Start(&hadc1);
        }
        if ((buf[0]=='h')||(buf[0]=='H')||(buf[0] == '?'))
        {
            addAperiodicJob(&transmitHelpString);
        }
    }
}

void read_ADC()
{
    if (HAL_OK == HAL_ADC_PollForConversion(&hadc1, 0))
    {
        // ADC ready
        char temp_str[15];
        uint32_t temp = HAL_ADC_GetValue(&hadc1);
        sprintf(temp_str, "T=%d\n\r", (int)temp);
        uartPrintString(temp_str, strlen(temp_str));
    }
}

int uartPrintString(char *str, int length) {
    int remaining_buffer;
    if(tx_tail >= tx_head) {
        remaining_buffer = TX_BUFFER_SIZE - tx_tail + tx_head - 1;
    }
    else {
        remaining_buffer = tx_head - tx_tail - 1;
    }

    if(length > remaining_buffer) {
        return 1;
    }

    // copy bytes
    int i;
    for(i = 0; i < length; i ++) {
        int write_loc = tx_tail + i;
        if(write_loc >= TX_BUFFER_SIZE) write_loc -= TX_BUFFER_SIZE;
        tx_buffer[write_loc] = str[i];
    }
    
    tx_tail += length;
    if(tx_tail >= TX_BUFFER_SIZE) {
        tx_tail -= TX_BUFFER_SIZE;
    }

    return 0;
}

void transmitUARTByte() {
    if(tx_head != tx_tail) {
        HAL_UART_Transmit(&huart2, &tx_buffer[tx_head], 1, 5);
        tx_head ++;
        if(tx_head >= TX_BUFFER_SIZE) {
            tx_head = 0;
        }
    }
}

void transmitHelpString() {
    // Send Help Message
    char msg_help[] = "This code monitors for blue/user button trigger, and reads ADC1 when asked with letter 't'\n\r";
    uartPrintString(msg_help, strlen(msg_help));
}

// sporadic functions
void respondToInput() {
    // wait 400 ms for debouncing
    if(HAL_GetTick() - last_trigger > 400) {
        last_trigger = HAL_GetTick();
        char msg[] = "Triggered!\n\r";
        uartPrintString(msg, strlen(msg));
    }
}

