
#include "sch_basic_prv.h"
#include "sch_basic_pub.h"

#define CLOCK_SPEED 84000000UL
//
// global array of periodic tasks
static struct Task tasks[MAX_NUM_TASKS];

// global array of aperiodic tasks
static void (*a_jobs[MAX_NUM_TASKS])();
static volatile int a_head = 0, a_tail = 0;

// private functions
void executeJob(struct Task *task);
void pollAperiodicServer();

// we will user timers 2-5, 9-12 as these are general purpose timers
// interrupt handlers

extern void TIM2_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[0].timer);
}

extern void TIM3_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[1].timer);
}

extern void TIM4_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[2].timer);
}

extern void TIM5_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[3].timer);
}

extern void TIM1_BRK_TIM9_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[4].timer);
}

extern void TIM1_UP_TIM10_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[5].timer);
}

extern void TIM1_TRG_COM_TIM11_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[6].timer);
}

extern void TIM8_BRK_TIM12_IRQHandler() {
    HAL_TIM_IRQHandler(&tasks[7].timer);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *tim_in) {
    if(tim_in->Instance == TIM2) {
        executeJob(&tasks[0]);
    }
    if(tim_in->Instance == TIM3) {
        executeJob(&tasks[1]);
    }
    if(tim_in->Instance == TIM4) {
        executeJob(&tasks[2]);
    }
    if(tim_in->Instance == TIM5) {
        executeJob(&tasks[3]);
    }
    if(tim_in->Instance == TIM9) {
        executeJob(&tasks[4]);
    }
    if(tim_in->Instance == TIM10) {
        executeJob(&tasks[5]);
    }
    if(tim_in->Instance == TIM11) {
        executeJob(&tasks[6]);
    }
    if(tim_in->Instance == TIM12) {
        executeJob(&tasks[7]);
    }
}


void initScheduler(int addPollingServer) {
    int i;

    for( i = 0; i < MAX_NUM_TASKS; i ++) {
        tasks[i].initialized = 0;
        tasks[i].running = 0;
        tasks[i].priority = 0;
    }
    
    tasks[0].timer.Instance = TIM2;
    tasks[1].timer.Instance = TIM3;
    tasks[2].timer.Instance = TIM4;
    tasks[3].timer.Instance = TIM5;
    tasks[4].timer.Instance = TIM9;
    tasks[5].timer.Instance = TIM10;
    tasks[6].timer.Instance = TIM11;
    tasks[7].timer.Instance = TIM12;
    // etc...

    // initialize timer settings for each timer
    HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
    HAL_NVIC_SetPriority(TIM3_IRQn, 1, 1);
    HAL_NVIC_SetPriority(TIM4_IRQn, 2, 2);
    HAL_NVIC_SetPriority(TIM5_IRQn, 3, 3);
    HAL_NVIC_SetPriority(TIM1_BRK_TIM9_IRQn, 4, 4);
    HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 5, 5);
    HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 6, 6);
    HAL_NVIC_SetPriority(TIM8_BRK_TIM12_IRQn, 7, 7);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
    HAL_NVIC_EnableIRQ(TIM3_IRQn);
    HAL_NVIC_EnableIRQ(TIM4_IRQn);
    HAL_NVIC_EnableIRQ(TIM5_IRQn);
    HAL_NVIC_EnableIRQ(TIM1_BRK_TIM9_IRQn);
    HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
    HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM11_IRQn);
    HAL_NVIC_EnableIRQ(TIM8_BRK_TIM12_IRQn);

    // set the lowest proirity job to be the aperiodic polling server
    addPeriodicTask(&pollAperiodicServer, 7, soft, 100, 0);
}

int addPeriodicTask(void (*job)(), int priority, enum LAXITY laxity, int period, int period_in_us) {
    uint32_t clock_divided;

    if(tasks[priority].initialized) {
        // this priority is already taken
        return -1;
    }

    switch(priority) {
        // high priority
        case 0:
            __TIM2_CLK_ENABLE();
            break;
        case 1:
            __TIM3_CLK_ENABLE();
            break;
        case 2:
            __TIM4_CLK_ENABLE();
            break;
        case 3:
            __TIM5_CLK_ENABLE();
            break;
        case 4:
            __TIM9_CLK_ENABLE();
            break;
        case 5:
            __TIM10_CLK_ENABLE();
            break;
        case 6:
            __TIM11_CLK_ENABLE();
            break;
        case 7:
            __TIM12_CLK_ENABLE();
            break;
        default:
            // unknown priority given
            return -2;
            
    }

    uint32_t prescaler = 10000;
    clock_divided = CLOCK_SPEED / 1000;
    if(period_in_us) {
        clock_divided /= 1000;
        prescaler = 100;
    }

    // set task struct member variables
    tasks[priority].job = job;
    tasks[priority].initialized = 1;
    tasks[priority].running = 0;
    tasks[priority].period = period;
    tasks[priority].period_in_us = period_in_us;
    tasks[priority].laxity = laxity;
    tasks[priority].priority = priority;

    // set timer registers
    tasks[priority].timer.Init.Prescaler = prescaler;
    tasks[priority].timer.Init.CounterMode = TIM_COUNTERMODE_UP;
    tasks[priority].timer.Init.Period = clock_divided * period / prescaler;
    tasks[priority].timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    tasks[priority].timer.Init.RepetitionCounter = 0;
    HAL_TIM_Base_Init(&tasks[priority].timer);
    HAL_TIM_Base_Start_IT(&tasks[priority].timer);

    return priority;
}

void removePeriodicTask(int id) {
    // frees the task and stops timer
    tasks[id].initialized = 0;
    HAL_TIM_Base_Stop_IT(&tasks[id].timer);
}

int addAperiodicJob(void (*job)()) {
    // check if queue is full
    if(a_tail + 1 == a_head || (a_tail + 1 == MAX_NUM_JOBS && a_head == 0)) {
        return 1;
    }

    a_jobs[a_tail] = job;
    a_tail ++;
    if(a_tail >= MAX_NUM_JOBS) {
        a_tail = 0;
    }

    return 0;
}

int addSporadicJob(void (*job)(), int deadline, int deadline_in_us) {
    int i;
    for(i = 0; i < MAX_NUM_TASKS; i ++) {
        if(tasks[i].initialized == 0) {
            tasks[i].is_sporadic = 1;
            addPeriodicTask(job, i, hard, deadline, deadline_in_us);
            return 0;
        }
    }
    return 1;
}

void executeJob(struct Task *task) {
    // if this task is already running and did not finish before deadline
    if(task->running) {
        if(task->laxity == soft) {
            // we don't do anything
        }
        else if (task->laxity == firm) {
            // set flag to cancel previous job
        }
        else {
            // if deadline is hard, crash the system.
            exit(1);
        }
    }

    task->running = 1;
    task->job();
    task->running = 0;
    if(task->is_sporadic) {
        removePeriodicTask(task->priority);
        tasks->is_sporadic = 0;
    }
}

void pollAperiodicServer() {
    if(a_head != a_tail) {
        (*a_jobs[a_head])();
        a_head ++;
        if(a_head >= MAX_NUM_JOBS) {
            a_head = 0;
        }
    } 
}

// ############################################################################
// ############################################################################




// ############################################################################
// ############################################################################
// Spacers
// ############################################################################
// ############################################################################



