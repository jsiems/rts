#ifndef _SCH_BASIC_PUB_H_
#define _SCH_BASIC_PUB_H_

/****************************************************************************
**	Includes (PUBLIC)
****************************************************************************/
#include <inttypes.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"

// uart used for debugging
UART_HandleTypeDef huart2;

/****************************************************************************
**	Constants, definies and typedefs  (PUBLIC)
****************************************************************************/

#define MAX_NUM_TASKS 8
#define MAX_NUM_JOBS 8

enum LAXITY {hard, firm, soft};

struct Task {
    enum LAXITY laxity;
    int running;
    int initialized;
    void (*job)();
    int priority;
    int period;
    int period_in_us;
    int is_sporadic;
    TIM_HandleTypeDef timer;
};

// PRE: global array of tasks called 'tasks' is declared of size 8
// POST: each task is set to not be initialized or running
// RETURN: N/A
// DESC: Initializes the global array of tasks
void initScheduler();

// PRE: global array tasks is initialized, timer with priority is available, laxity is valid,
//      period is in microseconds
// POST: job is added to task list with given priority, laxity and period
// PARAM job: pointer to job function to be executed
// PARAM priority: priority of task
// PARAM laxity: what to do with task if deadline is missed
// PARAM period: how often this task should execute
// RETURN: task id on success, -1 on error
// DESC: adds a task to the global task list
int addPeriodicTask(void (*job)(), int priority, enum LAXITY laxity, int period, int period_in_us);

// PRE: scheduler is initialized
// POST: aperiodic task is added assuming the job queue is not full
// PARAM job: pointer to function to be executed
// RETURN: 0 if successful, something else otherwise
// DESC: aperiodic task will be executed at some point in the future without a deadline
int addAperiodicJob(void (*job)());

// PRE: global task list is initialized
// POST: task with id is removed from tasks list, timer is stopped
// PARAM id: id of task to be removed
// RETURN: N/A
// DESC: Removes task of id from tasks list
void removePeriodicTask(int id);

// PRE: scheduler is initialized
// POST: sporadic job is added as a temporary periodic job
// PARAM job: pointer to function to be executed
// PARAM deadline: when the job should be finished by
// PARAM deadline_in_us: 1 for us, 0 for ms
// RETURN: 0 if successful, something else otherwise
// DESC: sporadic job will be executed after
int addSporadicJob(void (*job)(), int deadline, int deadline_in_us);

// ############################################################################
// ############################################################################
// Spacers
// ############################################################################
// ############################################################################


#endif // _SCH_BASIC_PUB_H_


