// main.c

#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#include "serial.h"

// Timeout Defines (ms)
#define START_DELAY 5000

#define BYTE_CODE 0xAA

char *portname = "/dev/ttyACM0";

int main() {
    char buf[10];
    int serial;
    struct timeval added, started;
    double time1, time2;
    printf("running\n");

    // initialize serial
    serial = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (serial < 0) {
        printf("error %d opening %s: %s\n", errno, portname, strerror(errno));
        return 1;
    }

    set_interface_attribs(serial, B115200, 0);
    set_blocking(serial, 1);

    printf("waiting for byte code\n");

    // wait for ADD_RESPONSE
    read(serial, buf, 1);
    gettimeofday(&added, 0);

    if((buf[0] & 0xFF) != BYTE_CODE) {
        printf("Error: invalid response received: 0x%x\nexpected 0x%x (BYTE_CODE)\n", buf[0], BYTE_CODE);
        return 1;
    }
    printf("received byte code, waiting for another\n");

    // wait for EXECUTING resposne
    read(serial, buf, 1);
    gettimeofday(&started, 0);

    if((buf[0] & 0xFF) != BYTE_CODE) {
        printf("Error: invalid response received: 0x%x\nExpected 0x%x (BYTE_CODE)\n", buf[0], BYTE_CODE);
        return 1;
    }
    printf("receieved byte code again\n");

    // calculate all the stuff
    // actual START time:
    time1 = (double)(started.tv_usec) / 1000000 +
           (double)(started.tv_sec);
    printf("actual start time: %f\n", time1);
    // expected start time:
    time2 = (double)(added.tv_usec) / 1000000 +
           (double)(added.tv_sec) + START_DELAY / 1000;
    printf("expected start time: %f\n", time2);
    // difference (ms)
    printf("difference(ms): %f\n", (time1 - time2) * 1000);

    printf("done\n");
}


